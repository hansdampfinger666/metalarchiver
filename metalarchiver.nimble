# Package

version       = "0.1.0"
author        = "al"
description   = "Metal Archives crawler and CD collection"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["metalarchiver"]


# Dependencies

requires "nim >= 2.0.0"
requires "db_connector >= 0.1.0"
