import os
import std/json
import std/strutils
import std/strscans
import std/uri
import std/paths
import db_connector/db_sqlite
import std/options
import std/sugar
import std/times

import logging
import scheduler
import files


const base_band_url =
  "https://www.metal-archives.com/search/ajax-advanced/searching/bands/?" &
  "bandName=$1" &
  "&genre=&country=$2" &
  "&yearCreationFrom=&yearCreationTo=&bandNotes=&status=&themes=&location=" &
  "&bandLabelName=&sEcho=1&iColumns=3&sColumns=&" &
  "iDisplayStart=$3" &
  "&iDisplayLength=" &
  "&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&_=1677610047657"
const base_release_url =
  "https://www.metal-archives.com/search/ajax-advanced/searching/albums/?" &
  "bandName=$1" &
  "&releaseTitle=$2" &
  "&releaseYearFrom=$3" &
  "&releaseMonthFrom=$4" &
  "&releaseYearTo=$5" &
  "&releaseMonthTo=$6" &
  "&country=$7" &
  "&location=$8" &
  "&releaseLabelName=&releaseCatalogNumber=&releaseIdentifiers=" &
  "&releaseRecordingInfo=&releaseDescription=&releaseNotes=&genre=&sEcho=1" &
  "&iColumns=3&sColumns=" &
  "&iDisplayStart=$9" &
  "&iDisplayLength=$10" &
  "&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&_=1669580470942"
const page_size = 200
const max_saved_responses = 10

type
  Request = object
    handle: int
    start: int
  CrawlerType* = enum
    bands
    releases
  Crawler* = object
    kind: CrawlerType
    sch: ptr Scheduler
    db: DbConn
    start = 0
    to = page_size
    total = 0
    progress = 0.0
    url: string
    init = true
    requests: seq[Request]
    responses: seq[string]


proc update_progress(crawler: var Crawler) =

  if (crawler.start == 0) or (crawler.total == 0):
    return

  crawler.progress = crawler.total / crawler.start


proc build_url(crawler: var Crawler) =

  crawler.url = case crawler.kind
    of bands: 
      base_band_url % ["", "", $crawler.start]
    of releases:
      base_release_url % ["", "", "", "", "", "", "", "", $crawler.start, ""]


proc init_crawler*(kind: CrawlerType, sch: ptr Scheduler, db: DbConn): Crawler =

  result.kind = kind
  result.sch = sch
  result.db = db

  let crawler_state = db.getRow(sql"""
    SELECT got_to, total FROM crawler_state
      WHERE crawler_kind = ?
    """,
    result.kind.ord()
  )

  if crawler_state[0] != "":
    result.start = crawler_state[0].parseInt()
    result.to = result.start

  if crawler_state[1] != "":
    result.total = crawler_state[1].parseInt()

  result.build_url()

  if result.start != 0:
    dbg_print("Crawler (kind = " & $kind &
      ") state found on database. Got_to: " & $result.start)
  else:
    dbg_print("No crawler (kind = " & $kind & ") got_to found on database.")

  if result.total != 0:
    dbg_print("Crawler (kind = " & $kind &
      ") total found on database. Total: " & $result.total)
  else:
    dbg_print("No crawler (kind = " & $kind & ") total found on database.")


proc get_id_name_url(input: string): tuple[id: string, name: string,
    url: string] =

  if input.scanf("<a href=\"$+\">$+<", result.url, result.name):
    result.id = result.url.parseUri().path.Path().extractFilename().string()


proc get_id(input: string): string =

  if input.scanf("<a href=\"$+\"", result):
    result = result.parseUri().path.Path().extractFilename().string()


proc parse_response(crawler: Crawler, resp: string): tuple[total_records: int,
    records: seq[seq[string]]] =

  let json = resp.parseJson()
  result.total_records = json["iTotalRecords"].getInt()

  case crawler.kind
  of bands:
    if json["aaData"].len == 0:
      dbg_print("Crawler (kind = " & $crawler.kind &
        ") response does not contain any records.")
      return

    for row in json["aaData"]:
      result.records.add(default(seq[string]))
      var idx = 0
      var id, name, url, genre, country, location: string

      for col in row:
        case idx
        of 0:
          (id, name, url) = col.getStr().get_id_name_url()
        of 1:
          genre = col.getStr()
        of 2:
          country = col.getStr()
        else:
          discard
        inc idx

      result.records[^1].add(id)
      result.records[^1].add(name)
      result.records[^1].add(url)
      result.records[^1].add(genre)
      result.records[^1].add(country)
      result.records[^1].add(location)

  of releases:
    if json["aaData"].len == 0:
      dbg_print("Crawler (kind = " & $crawler.kind &
        ") response does not contain any records.")
      return

    for row in json["aaData"]:
      result.records.add(default(seq[string]))
      var idx = 0
      var id, band_id, name, url, rtype: string
      for col in row:
        case idx
        of 0:
          band_id = get_id(col.getStr())
        of 1:
          (id, name, url) = col.getStr().get_id_name_url()
        of 2:
          rtype = col.getStr()
        else:
          discard
        inc idx

      result.records[^1].add(id)
      result.records[^1].add(band_id)
      result.records[^1].add(name)
      result.records[^1].add(url)
      result.records[^1].add(rtype)


proc write_to_db(crawler: Crawler, record: seq[string]) =

  case crawler.kind
  of bands:
    try:
      assert(record.len <= 6)
    except AssertionDefect:
      let msg = "Record for crawler (kind = " & $crawler.kind &
        ") does not comply with database table: " & $record
      dbg_print("Record does not comply with database table: " & $record)
      log_fatal(msg)
      return

    crawler.db.exec(sql"""
      INSERT OR REPLACE INTO bands 
      VALUES (?, ?, ?, ?, ?, ?) 
      """,
      record[0], # id
      record[1], # name
      record[2], # url
      record[3], # genre
      record[4], # country
      "" # location
    )

  of releases:
    try:
      assert(record.len <= 5)
    except AssertionDefect:
      let msg = "Record for crawler (kind = " & $crawler.kind &
        ") does not comply with database table: " & $record
      dbg_print(msg)
      log_fatal(msg)
      return

    crawler.db.exec(sql"""
      INSERT OR REPLACE INTO releases
      VALUES (?, ?, ?, ?, ?) 
      """,
      record[0], # id
      record[1], # band_id
      record[2], # name
      record[3], # url
      record[4], # type
    )


proc start_idx_min(crawler: Crawler): int =

  result = int.high

  for req in crawler.requests:
    if req.start < result:
      result = req.start


proc persist_crawler_state(crawler: Crawler, got_to: int) =

  if got_to == crawler.start_idx_min():
    dbg_print("Persisting crawler (kind = " & $crawler.kind &
      ") state, got_to: " & $got_to)

    crawler.db.exec(sql"""
        INSERT OR REPLACE INTO crawler_state
          VALUES (?, ?, ?)
      """,
      crawler.kind.ord(),
      got_to + page_size - 1,
      crawler.total
    )


proc save_response(crawler: Crawler, req: Request) =

  save_rotating_file(
    dir = "./",
    file = $getTime() & "_" & $req.start & "_" & $(req.start + page_size) & 
      ".json",
    max_files = max_saved_responses,
    content = crawler.responses[^1]
  )


proc handle_responses(crawler: var Crawler) =

  for idx, req in crawler.requests:
    let resp = crawler.sch[].get_response(req.handle)

    if resp.isSome():
      dbg_print("Got response for crawler (kind = " & $crawler.kind & ").")
      crawler.responses.add(resp.get())

      when not defined(prod):
        crawler.save_response(req)

      var records: seq[seq[string]]
      (crawler.total, records) = crawler.parse_response(crawler.responses[^1])

      for record in records:
        crawler.write_to_db(record)

      crawler.persist_crawler_state(req.start)
      crawler.requests.delete(idx)
      break
    else:
      break


template check_crawl_condition(cond: untyped) =

  if (cond):
    dbg_print("Crawler (kind = " & $crawler.kind & ") conditions not met.")
    dbg_print(dumpToString(cond))
    return


proc crawl*(crawler: var Crawler, number_requests = int.high) =

  const default_wait_for_slots_ms = 10
  var current_request = 0
  var slots = 0

  while true:
    when not defined(prod):
      warn_len("Crawler(kind = " & $crawler.kind & ").requests", 
        crawler.requests)
      warn_len("Crawler(kind = " & $crawler.kind & ").responses", 
        crawler.responses)

    if crawler.requests.len > 1_000:
      dbg_print("Crawler(kind = " & $crawler.kind & ").request.len over 1,000")

    check_crawl_condition((current_request + 1) > number_requests)
    check_crawl_condition(current_request + 1 > int.high)
    check_crawl_condition((crawler.total != 0) and
      ((crawler.to - crawler.total) > page_size))

    slots = crawler.sch[].request_queue_slots(number_requests -
      current_request)
    var wait_for_slots_ms = default_wait_for_slots_ms

    while slots == 0:
      sleep(wait_for_slots_ms)
      slots = crawler.sch[].request_queue_slots(number_requests -
        current_request)
      inc wait_for_slots_ms, default_wait_for_slots_ms

    wait_for_slots_ms = default_wait_for_slots_ms

    for slot in 1..slots:
      if crawler.init == true:
        crawler.init = false
      else:
        crawler.start = crawler.to
        crawler.update_progress()
        dbg_print("Updated crawler (kind = " & $crawler.kind & ") start: " &
          $crawler.start)
        inc crawler.to, page_size
        crawler.build_url()

      crawler.requests.add(
        Request(
          handle: crawler.sch[].add_to_queue(crawler.url),
          start: crawler.start 
        )
      )

    inc current_request, slots
    crawler.handle_responses()
