import std/terminal


proc del_lines*(lines = 1) =

  cursorUp(lines)
  eraseLine()


proc enter_cont* =

  echo "press Enter to continue"

  if readLine(stdin) == "\n":
    del_lines(2)


proc echo_del*(lines: seq[string]) =

  for line in lines:
    echo line
    del_lines()
