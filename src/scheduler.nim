import std/deques
import std/typedthreads
import std/locks
import std/asyncdispatch
import std/httpclient
import std/options
import std/times
import os

import logging
import watch


type
  Request = object
    handle: int
    url: string
  Response = object
    handle: int
    payload: Future[string]
  Scheduler* = object
    lock*: Lock
    handle_count = 0
    max_queue_slots {.guard: lock.}: int
    idle_runs = 0
    queue {.guard: lock.}: Deque[Request]
    response_futs {.guard: lock.}: seq[Response]
    interrupt {.guard: lock.} = false
    watch: StopWatch
    tick_rate = initDuration(0)

var scheduler_thread*: Thread[ptr Scheduler]


proc init_scheduler*(tick_rate: Duration): Scheduler {.thread.} =

  initLock(result.lock)
  result.tick_rate = tick_rate

  withLock result.lock:
    result.max_queue_slots = 10
    result.queue = initDeque[Request]()


proc stop_loop*(sch: var Scheduler) {.thread.} =

  {.cast(gcsafe).}:
    withLock sch.lock:
      sch.interrupt = true


proc request_queue_slots*(sch: var Scheduler,
    requested_slots = sch.max_queue_slots): int {.thread.} =

  {.cast(gcsafe).}:
    withLock sch.lock:
      if sch.idle_runs > 0:
        dbg_print("Scheduler ran idle for amount of runs " & $sch.idle_runs)
      inc sch.max_queue_slots, sch.idle_runs
      sch.idle_runs = 0
      result = sch.max_queue_slots - sch.queue.len

      if result >= requested_slots:
        result = requested_slots


proc add_to_queue*(sch: var Scheduler, url: string): int {.thread.} =

  {.cast(gcsafe).}:
    inc sch.handle_count
    result = sch.handle_count

    withLock sch.lock:
      sch.queue.addFirst(Request(handle: result, url: url))


proc get_response*(sch: var Scheduler, response_handle: int): Option[string]
    {.thread.} =

  {.cast(gcsafe).}:
    withLock sch.lock:
      for idx, resp in sch.response_futs:
        if (resp.handle == response_handle) and resp.payload.finished():
          result = some(resp.payload.read())
          sch.response_futs.delete(idx)
          return

      result = none(string)


proc get_web_content(url: string): Future[string] {.async.} =

  let client = newAsyncHttpClient()
  defer: client.close()
  result = await client.getContent(url)


proc get_queue_size(sch: var Scheduler): int {.thread.} =

  {.cast(gcsafe).}:
    withLock sch.lock:
      result = sch.queue.len


proc get_response_futs_size(sch: var Scheduler): int {.thread.} =

  {.cast(gcsafe).}:
    withLock sch.lock:
      result = sch.response_futs.len


proc run_loop*(sch: ptr Scheduler) {.thread.} =

  {.cast(gcsafe).}:

    while true:
      when not defined(prod):
        warn_len("Scheduler.queue: ", sch[].get_queue_size())
        warn_len("Scheduler.response_futs", sch[].get_response_futs_size())

      let time_passed = sch.watch.read()

      if (not sch.watch.running()) or (time_passed.get() >= sch.tick_rate):
        sch.watch.start()
        dbg_print("Scheduler tick.")
      else:
        sleep((sch.tick_rate - time_passed.get()).inMilliseconds)

      withLock sch.lock:
        if sch.interrupt == true:
          dbg_print("Read interrupt.")
          return

        if sch.queue.len > 0:
          dbg_print("Entries in queue: " & $sch.queue.len)
          let req = sch.queue.popLast()

          sch.response_futs.add(
            Response(
              handle: req.handle,
              payload: get_web_content(req.url)
            )
          )
        elif sch.response_futs.len == 0:
          inc sch.idle_runs

        for resp in sch.response_futs:
          if not resp.payload.finished():
            try:
              poll(timeout = 1)
            except ValueError:
              dbg_print("Value error when polling.")
              discard
            break
