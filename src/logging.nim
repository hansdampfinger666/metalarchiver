import os
import std/times

import files


const log_dir = "./logs"
const max_logs = 50
const default_max_len = 1_000


proc setup_logging*() =

  discard existsOrCreateDir(log_dir)


proc dbg_print*(str: string) =

  echo "[", getThreadID(), "][", getTime(), "] ", str


template warn_len*[T](collection_name: string, collection: T,
    max_len = default_max_len) =

  if collection.len > max_len:
    dbg_print(collection_name & " is larger than " & $max_len)


template warn_len*(collection_name: string, collection_len: int,
    max_len = default_max_len) =

  if collection_len > max_len:
    dbg_print(collection_name & " is larger than " & $max_len)


proc log_fatal*(e: ref Exception) =

  save_rotating_file(log_dir, $getTime() & "_erorr.log", max_logs, e.msg)


proc log_fatal*(msg: string) =

  save_rotating_file(log_dir, $getTime() & "_erorr.log", max_logs, msg)
