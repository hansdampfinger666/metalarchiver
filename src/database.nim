import os
import std/times
import db_connector/db_sqlite

import logging
import files


const db_backup_dir = "./db_backups"
const db_file = "db.db"
const max_db_backups = 10


proc backup_database() =

  discard existsOrCreateDir(db_backup_dir)
  save_rotating_file(db_backup_dir, $getTime() & "_db.bak", max_db_backups,
    db_file)


proc init_bands_tab(db: DbConn) =

  db.exec(sql"""
    CREATE TABLE bands (
      id INTEGER PRIMARY KEY,
      name VARCHAR(100),
      url VARCHAR(100),
      genre VARCHAR(100),
      country VARCHAR(100),
      location VARCHAR(100)
    ) 
  """)


proc init_releases_tab(db: DbConn) =

  db.exec(sql"""
    CREATE TABLE releases (
      id INTEGER PRIMARY KEY,
      band_id INTEGER,
      name VARCHAR(100),
      url VARCHAR(100),
      type VARCHAR(100)
    ) 
  """)


proc init_crawl_status_tab(db: DbConn) =

  db.exec(sql"""
    CREATE TABLE crawler_state (
      crawler_kind INTEGER PRIMARY KEY,
      got_to INTEGER,
      total INTEGER
    ) 
  """)


proc init_database*: DbConn =

  when not defined(prod):
    backup_database()
    removeFile(db_file)

  if fileExists(db_file):
    dbg_print("Database exists.")
    backup_database()
    result = open(db_file, "", "", "")
    return
  else:
    dbg_print("Database does not exist. Initializing.")
    result = open(db_file, "", "", "")
    result.init_bands_tab()
    result.init_releases_tab()
    result.init_crawl_status_tab()


proc set_test_data(db: DbConn) =

  when defined(prod):
    return

  db.exec(sql"""
      INSERT INTO bands (id, name, url, genre, country, location)
      VALUES (?, ?, ?, ?, ?, ?)
    """,
    3540344201,
    "Abhorrent Castigation",
    "https://www.metal-archives.com/bands/Abhorrent_Castigation/3540344201",
    "Brutal Death Metal",
    "Germany",
    "Augsburg, Bavaria / Bremen (early); Landsberg am Lech, Bavaria / Bremen (later)"
  )


proc get_bands*(db: DbConn): seq[Row] =

  return db.getAllRows(sql"SELECT * FROM bands")
