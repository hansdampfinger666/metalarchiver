import os
import std/times
import std/paths


type
  FileTime = object
    path: string
    last_changed: Time


template save_rotating_file_core =

  assert dirExists(dir)
  var saved_files = 0
  var oldest_file_time: FileTime
  let ext = file.Path.splitFile().ext

  for current_file in walkDir(dir):
    if current_file.path.splitFile().ext == ext:
      inc saved_files
      let last_changed = current_file.path.getFileInfo().lastWriteTime

      if oldest_file_time.last_changed == default(Time) or
          last_changed < oldest_file_time.last_changed:
        oldest_file_time.path = current_file.path
        oldest_file_time.last_changed = last_changed

  if saved_files >= max_files:
    removeFile(oldest_file_time.path)


proc save_rotating_file*(dir: string, file: string, max_files: int,
    content: string) =

  save_rotating_file_core()
  writeFile(dir & "/" & file, content)


proc save_rotating_file*(dir: string, file: string, max_files: int,
    file_to_copy: Path) =

  save_rotating_file_core()
  copyFile(file_to_copy.string, dir & "/" & file)
