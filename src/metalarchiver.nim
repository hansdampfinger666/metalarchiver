import os
import db_connector/db_sqlite
import std/asyncdispatch

import logging
import database
import web_utils
import scheduler
import crawler


const crawl_opts_str = ["--crawl", "-c"]
when not defined(prod):
  const number_requests = 100_000
else:
  const number_requests = int.high

proc main(params: seq[string]) =
  setup_logging()
  var crawl: bool
  for param in params:
    if param in crawl_opts_str:
      crawl = true
      break
  if crawl:
    try:
      if not has_internet() and not retry_internet():
        dbg_print("Cannot crawl.")
        return
      let db = init_database()
      let tick_rate = waitFor(read_robots_txt_delay())
      var sch = init_scheduler(tick_rate)
      var band_crawler = init_crawler(bands, addr sch, db)
      var release_crawler = init_crawler(releases, addr sch, db)
      createThread(scheduler_thread, run_loop, addr sch)
      dbg_print("Robot.txt tick rate: " & $tick_rate)
      band_crawler.crawl(number_requests)
      release_crawler.crawl(number_requests)
      sch.stop_loop()
      db.close()
      joinThread(scheduler_thread)
    except Exception as e:
      log_fatal(e)

main(commandLineParams())
