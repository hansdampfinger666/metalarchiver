import std/options
import std/times


type
  StopWatch* = object
    start: DateTime = default(DateTime)
    finish: DateTime = default(DateTime)


proc running*(watch: StopWatch): bool =

  watch.start != default(DateTime)


proc start*(watch: var StopWatch) =

  watch.start = now()


proc start_new*(watch: var StopWatch) =

  watch = default(StopWatch)
  watch.start()


proc read*(watch: StopWatch): Option[Duration] =

  if watch.start == default(DateTime):
    none(Duration)
  else:
    some(now() - watch.start)


proc finish*(watch: var StopWatch): Duration =

  watch.finish = now()
  result = watch.finish - watch.start
  watch = default(StopWatch)
