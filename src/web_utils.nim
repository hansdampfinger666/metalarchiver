import os
import std/httpclient
import std/asyncdispatch
import std/strutils
import std/strscans
import std/times

import logging

const inet_test_urls = [
  "https://www.swisscows.com",
  "https://www.duckduckgo.com",
]
const ma_robot_url = "https://www.metal-archives.com/robots.txt"
const default_robot_delay = initDuration(seconds = 3)
const default_inet_retry_delay_ms = 500
const default_retries = 20


proc has_internet*(urls = inet_test_urls): bool =

  let client = newHttpClient()
  defer: client.close()

  for test_url in urls:
    try:
      discard client.get(test_url)
      result = true
      dbg_print("Internet connection okay.")
      return
    except: discard

  dbg_print("No internet connection.")


proc retry_internet*(retries = default_retries,
    delay = default_inet_retry_delay_ms): bool =

  for retry in 1..default_retries:
    dbg_print("Retrying.")

    if has_internet():
      return
    else:
      sleep(default_inet_retry_delay_ms)


proc read_robots_txt_delay*(robot_url = ma_robot_url): Future[Duration]
    {.async.} =

  let client = newAsyncHttpClient()
  defer: client.close()
  let content = await client.getContent(robot_url)
  var dur_int: int

  for line in content.splitLines():
    if line.scanf("Crawl-delay: $i", dur_int):
      result = initDuration(seconds = dur_int)
      return

  result = default_robot_delay
